# Imagen base
FROM node:latest

# Directorio de la app
WORKDIR /app

# Copio archivos
ADD build/es5-bundled /app/build/es5-bundled
ADD package.json /app
ADD server.js /app

# Dependencias
RUN npm install
# RUN apt-get update
# RUN apt-get install -y vim

# Puerto
EXPOSE 2000

# Command
CMD ["npm", "start"]
