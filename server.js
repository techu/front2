var express = require('express'),
  app = express(),
  port = process.env.PORT || 2000;

var path = require('path');

app.use(express.static(__dirname + '/build/es5-bundled'));

app.listen(port);

console.log('BBVA TechU started on: ' + port);

app.get('/', function(req, res) {
    // res.sendFile(path.join(__dirname, 'index.html'));
    res.sendFile('index.html', {root: '.'});
});
